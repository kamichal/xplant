import pytest

from xplant._xml_attributes import XmlAttributes


class AttrsProcessor(XmlAttributes):
    attribute_precedence_rank = {
        "one": 1,
        "two": 2,
        "last": 120,
    }
    attribute_name_substitutes = {
        "klass": "class",
        "def_": "def",
    }


ATTRIBUTE_PROCESSING_TEST = [
    ([], {}, {}, ""),
    (["one"], {}, {"one": None}, " one"),
    ([("it", "is")], {}, {"it": "is"}, ' it="is"'),
    ([], {"it": "is"}, {"it": "is"}, ' it="is"'),
    ([("it", 4)], {"it": "collides"}, {"it": "collides"}, ' it="collides"'),
    (["other", ("two", 22), ("thing", 0), "last"], {}, {"other": None, "thing": 0, "two": 22, "last": None},
     ' two="22" other thing="0" last'),
    (["other", "two", "some", "one"], {}, {"one": None, "other": None, "some": None, "two": None},
     " one two other some"),
    ([("klass", "k")], {"def_": "sub", "klass": 3}, {"class": 3, "def": "sub", "klass": "k"},
     ' class="3" def="sub" klass="k"'),
]


@pytest.mark.parametrize("args, kwargs, expected_dict, expected_str", ATTRIBUTE_PROCESSING_TEST)
def test_attributes_sorting(args, kwargs, expected_dict, expected_str):
    processor = AttrsProcessor(*args, **kwargs)

    assert isinstance(processor, dict)
    assert processor == expected_dict
    assert str(processor) == expected_str


def test_badly_typed():
    with pytest.raises(ValueError, match=r"Attribute argument must be tuple of 2 elements \(name, value\)."):
        AttrsProcessor(("bad", "number", "of elements"))


def test_bad_types():
    with pytest.raises(ValueError, match=r"Couldn't make an attribute & value pair out of <object object"):
        AttrsProcessor(object())


def test_namespace_attr_substitution():
    assert AttrsProcessor("one", **dict(xmlns_svg="svg_ns", xmlns_xlink="xlink_ns")) == {
        'one': None,
        'xmlns:svg': 'svg_ns',
        'xmlns:xlink': 'xlink_ns'
    }

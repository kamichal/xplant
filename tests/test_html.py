from xplant import html5_plant


def test_html():
    css = """
        ul.navigation {
            list-style-type: none;
            margin: 0;
            padding: 0;
            overflow: hidden;
            background-color: #f2fff5;
        }
        li {
          float: left;
        }
        li a {
          display: block;
          color: white;
          text-align: center;
          padding: 14px 16px;
          text-decoration: none;
        }
        code {
            background-color: #ded;
            padding: 4px;
            border-radius: 3px;
        }
    """
    navigation = [
        ("Home", "../home.html", "red"),
        ("Things", "stuff.html", "green"),
        ("About", "../about.html", "blue"),
    ]
    text = [
        "This page has ben generated with python's <code> xplant.html.html5_plant </code>.",
        "Enjoy pure pythonic <code>1:1 python -> xml</code> translation.",
        "break",
        "Did you ever had hard times with learning <code>HTML template language</code>? ",
        "It's a crude way to mix HTML with any logics like iterators, classes, conditions.",
        "break",
        "You know what? You already have all of it (and much more) in <code>python</code>! ",
        "HTML templates is a blind alley. HTML does not miss server-side scripting.",
        "The python miss a good HTML generator not vice versa.",
    ]

    x = html5_plant()
    with x.html():
        with x.head():
            x.meta(charset="utf-8")
            x.meta(http_equiv="Content-Security-Policy")
            x.line("title", "no templates, no headache")

            with x.style():
                x.text(css)

        with x.body(style="margin:28px;"):
            with x.header():
                x.line("h2", "XPLANT", id="title")
                x.line("h4", "It makes good things for you")

            x.comment("HERE COMES THE NAVIGATION")
            with x.ul(id="navigation"):
                x.comment("CHECK OUT THIS LIST")
                for name, link_url, link_color in navigation:
                    with x.li():
                        with x.a(href=link_url, style="color:%s;" % link_color):
                            x.text("%s in %s" % (name, link_color))

            x.comment("HERE COMES MAIN SECTION")
            with x.main(style="margin:20px;"):
                for paragraph in text:
                    with x.p():
                        if paragraph == "break":
                            x.br()
                        else:
                            x.text(paragraph)

    assert str(x) == """\
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8" />
    <meta http-equiv="Content-Security-Policy" />
    <title>no templates, no headache</title>
    <style>
        ul.navigation {
            list-style-type: none;
            margin: 0;
            padding: 0;
            overflow: hidden;
            background-color: #f2fff5;
        }
        li {
          float: left;
        }
        li a {
          display: block;
          color: white;
          text-align: center;
          padding: 14px 16px;
          text-decoration: none;
        }
        code {
            background-color: #ded;
            padding: 4px;
            border-radius: 3px;
        }
    </style>
  </head>
  <body style="margin:28px;">
    <header>
      <h2 id="title">XPLANT</h2>
      <h4>It makes good things for you</h4>
    </header>
    <!-- HERE COMES THE NAVIGATION -->
    <ul id="navigation">
      <!-- CHECK OUT THIS LIST -->
      <li><a href="../home.html" style="color:red;">Home in red</a></li>
      <li><a href="stuff.html" style="color:green;">Things in green</a></li>
      <li><a href="../about.html" style="color:blue;">About in blue</a></li>
    </ul>
    <!-- HERE COMES MAIN SECTION -->
    <main style="margin:20px;">
      <p>This page has ben generated with python's <code> xplant.html.html5_plant </code>.</p>
      <p>Enjoy pure pythonic <code>1:1 python -> xml</code> translation.</p>
      <p><br /></p>
      <p>Did you ever had hard times with learning <code>HTML template language</code>? </p>
      <p>It's a crude way to mix HTML with any logics like iterators, classes, conditions.</p>
      <p><br /></p>
      <p>You know what? You already have all of it (and much more) in <code>python</code>! </p>
      <p>HTML templates is a blind alley. HTML does not miss server-side scripting.</p>
      <p>The python miss a good HTML generator not vice versa.</p>
    </main>
  </body>
</html>
"""


def test_repin_children():
    x1 = html5_plant()
    x2 = html5_plant()

    with x1.div(id="one"):
        with x1.div(id="two"):
            with x1.h4(id="idh4"):
                pass

        with x1.p():
            x1.text("paragraph text")

    with x1.div(id="next"):
        x1.text("second div")

    with x2.html():
        with x2.head():
            pass

        with x2.body(klass="that_klass"):
            x2.text("before replant")
            x2.replant(x1)
            x2.text("after replant")
        with x2.footer():
            x2.text("Added at the end")

    assert str(x2) == """\
<!DOCTYPE html>
<html>
  <head></head>
  <body class="that_klass">
    before replant
    <div id="one">
      <div id="two"><h4 id="idh4"></h4></div>
      <p>paragraph text</p>
    </div>
    <div id="next">second div</div>
    after replant
  </body>
  <footer>Added at the end</footer>
</html>
"""

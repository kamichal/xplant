from xplant import svg_plant


def test_empty_svg():
    svg = svg_plant()

    assert str(svg) == """\
<?xml version="1.0" encoding="utf-8" standalone="no"?>
<!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">
"""


def test_simple_svg():
    svg = svg_plant()

    with svg.node('svg'):
        with svg.node('g', id="rects"):
            svg.leaf('rect', x=0, y=12, height=10, width=13)
            svg.leaf('rect', x=0, y=12, height=10, width=13)
        svg.leaf('polyline', stroke="#AF3A46", stroke_width=4.9763,
                 points="238, 171.1, 245.2, 109, 260.4, 86.9")
        assert str(svg) == """\
<?xml version="1.0" encoding="utf-8" standalone="no"?>
<!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">
<svg>
  <g id="rects">
    <rect height="10" width="13" x="0" y="12" />
    <rect height="10" width="13" x="0" y="12" />
  </g>
  <polyline points="238, 171.1, 245.2, 109, 260.4, 86.9" stroke="#AF3A46" stroke-width="4.9763" />
</svg>
"""

from xplant._six import str_types


def pytest_assertrepr_compare(op, left, right):
    """
    It's quite common that we compare string representation of documents.
    Native pytest's comparison failure representation is at least unreadable for this purpose.
    Printing the content in case of failure makes development and debug cycle longer.
    This prints compared strings only in case of test's failure.
    """
    if op == "==" and isinstance(left, str_types) and isinstance(right, str_types):
        print("---")
        print(left)
        print("---")
        print("supposed to be:")
        print("---")
        print(right)
        print("---")

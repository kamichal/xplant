import pytest

from xplant import NodeBase


def test_raises_abstract():
    with pytest.raises(NotImplementedError) as e:
        NodeBase(None).string_items(0)
    assert str(e.value) == """\
NodeBase.string_items is an abstract method.

It's supposed to:
 - be a generator yielding string items,
 - compose document representation of that node with ''.join(node.string_items(0)),
 - care for document's line breaks, markup and indentation.
It needs to be implemented in derived class 'NodeBase'.
"""

from xplant import yaml_plant

""" This can be an example of basic usage. """


def test_empty_document():
    x = yaml_plant()
    assert str(x) == ""


def test_adding_leaf():
    x = yaml_plant()
    x.node("deleaf")
    x.node("ateaf")
    assert str(x) == """\
 - deleaf
 - ateaf
"""


def test_adding_node():
    x = yaml_plant()
    with x.node("denode"):
        x.node("ateaf")
        x.node("madd")
        with x.node("kak"):
            x.node("node")
            with x.node("nested kak"):
                x.node("nested node")
                x.node("nested leaf2")
    with x.node("pantal"):
        pass
    x.node("end")

    assert str(x) == """\
 - denode:
   - ateaf
   - madd
   - kak:
     - node
     - nested kak:
       - nested node
       - nested leaf2
 - pantal
 - end
"""


def test_example():
    x = yaml_plant()

    with x.node("level_0_A"):
        with x.node("level_1_A"):
            for line in ("A", "B", "C"):
                with x.node("thing_called_{}".format(line)):
                    if line == "B":
                        x.node("what a?")

        with x.node("level_1_B"):
            x.node("that thing is a leaf")
            x.node("this one as well")

    with x.node("level_0_B"):
        pass

    assert str(x) == """\
 - level_0_A:
   - level_1_A:
     - thing_called_A
     - thing_called_B:
       - what a?
     - thing_called_C
   - level_1_B:
     - that thing is a leaf
     - this one as well
 - level_0_B
"""

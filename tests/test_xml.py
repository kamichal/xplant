import pytest

from xplant import xml_plant


@pytest.fixture
def x():
    return


def test_empty_doc():
    x = xml_plant()
    assert str(x) == '<?xml version="1.0" encoding="utf-8"?>\n'


def test_tag():
    x = xml_plant()
    with x.node("that_wonder"):
        pass
    assert str(x) == """\
<?xml version="1.0" encoding="utf-8"?>
<that_wonder></that_wonder>
"""


def test_stag():
    x = xml_plant()
    x.leaf("that_wonder")
    assert str(x) == """\
<?xml version="1.0" encoding="utf-8"?>
<that_wonder />
"""


def test_stag_2():
    x = xml_plant()
    x.leaf("one")
    x.leaf("two")
    assert str(x) == """\
<?xml version="1.0" encoding="utf-8"?>
<one />
<two />
"""


def test_text():
    x = xml_plant()
    x.text("that text\n even with\n multiple lines")
    assert str(x) == """\
<?xml version="1.0" encoding="utf-8"?>
that text
 even with
 multiple lines
"""


def test_text_in():
    x = xml_plant()
    with x.node("div"):
        x.text("that text\n even with\n multiple lines")

    assert len(x.children) == 1
    assert str(x) == """\
<?xml version="1.0" encoding="utf-8"?>
<div>that text
 even with
 multiple lines</div>
"""


def test_almost_empty_doc_1():
    x = xml_plant()
    with x.node("k"):
        pass
    assert str(x) == """\
<?xml version="1.0" encoding="utf-8"?>
<k></k>
"""


def test_almost_empty_doc_2():
    x = xml_plant()
    with x.node("k"):
        pass
    with x.node("jot"):
        pass
    assert str(x) == """\
<?xml version="1.0" encoding="utf-8"?>
<k></k>
<jot></jot>
"""


def test_from_readme():
    x = xml_plant()
    with x.node("section_a", attribute_1=1):
        with x.node("nested_1", empty=True):
            pass
        with x.node("nested_2"):
            x.comment("Can handle also comments.")
            for number in range(3):
                x.leaf("a_number_{:02}".format(number), num=number)

    assert str(x) == """\
<?xml version="1.0" encoding="utf-8"?>
<section_a attribute_1="1">
  <nested_1 empty="true"></nested_1>
  <nested_2>
    <!-- Can handle also comments. -->
    <a_number_00 num="0" />
    <a_number_01 num="1" />
    <a_number_02 num="2" />
  </nested_2>
</section_a>
"""


def test_node_as_plant():
    x = xml_plant()
    with x.node("thing", "arg1", "arg2", kwarg="kw"):
        x.leaf("leaf")

    assert x._NodeBase__parent is None
    assert str(x) == """\
<?xml version="1.0" encoding="utf-8"?>
<thing arg1 arg2 kwarg="kw"><leaf /></thing>
"""


def test_node_as_plant_nested():
    x = xml_plant()
    assert x._NodeBase__parent is None

    with x.node("plant_1", "arg", kwarg="kw"):
        with x.node("thing1", "arg1", kwarg="kw1") as thing1:
            with x.node("sub_in_thing1"):
                pass

        with x.node("thing2", "arg2", kwarg="kw2"):
            x.leaf("leaf1", "argument")
            x.leaf("leaf2", kwarg=2)
            x.leaf("leaf3")
    with x.node("plant_2"):
        x.leaf("thing_c2")
    x.leaf("child_3")

    assert str(thing1) == '<thing1 arg1 kwarg="kw1"><sub_in_thing1></sub_in_thing1></thing1>'
    assert str(x) == """\
<?xml version="1.0" encoding="utf-8"?>
<plant_1 arg kwarg="kw">
  <thing1 arg1 kwarg="kw1"><sub_in_thing1></sub_in_thing1></thing1>
  <thing2 arg2 kwarg="kw2">
    <leaf1 argument />
    <leaf2 kwarg="2" />
    <leaf3 />
  </thing2>
</plant_1>
<plant_2><thing_c2 /></plant_2>
<child_3 />
"""

    thing1.leaf("leaf_in_thing_1")
    assert str(thing1) == """\
<thing1 arg1 kwarg="kw1">
  <sub_in_thing1></sub_in_thing1>
  <leaf_in_thing_1 />
</thing1>"""

    assert str(x) == """\
<?xml version="1.0" encoding="utf-8"?>
<plant_1 arg kwarg="kw">
  <thing1 arg1 kwarg="kw1">
    <sub_in_thing1></sub_in_thing1>
    <leaf_in_thing_1 />
  </thing1>
  <thing2 arg2 kwarg="kw2">
    <leaf1 argument />
    <leaf2 kwarg="2" />
    <leaf3 />
  </thing2>
</plant_1>
<plant_2><thing_c2 /></plant_2>
<child_3 />
"""

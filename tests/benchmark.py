import StringIO
import cProfile
import datetime
import pstats
from contextlib import contextmanager

import django
import jinja2
import yattag
from django import template
from django.conf import settings

from xplant import html5_plant


@contextmanager
def time_it(name, maximum=None):
    begin = datetime.datetime.now()
    yield
    end = datetime.datetime.now()
    elapsed = int((end - begin).total_seconds() * 1e6)
    print("{} took {} microseconds".format(name, elapsed))

    if maximum and elapsed > maximum:
        raise RuntimeError("{} took {}us it's more than expected {}us".format(name, elapsed, maximum))


numbers = [0x1000 + i * 3 for i in range(80)]
my_list = ["item #%02d: %s is %s in hex" % (i, n, hex(n)) for i, n in enumerate(numbers)]

my_string = "the numbers are: " + ", ".join(hex(n) for n in numbers)
j_template = """\
<h3> This is the start of my child template</h3>
<br>
<p>My string: {{my_string}}</p>
<p>Value from the list: {{my_list[3]}}</p>
<p>Loop through the list:</p>
<ul>
  {% for n in my_list %}<li>{{n}}</li>
  {% endfor %}
</ul>
<h3> This is the end of my child template</h3>
"""

params = dict(my_string=my_string, my_list=my_list)
t = jinja2.Template(j_template)
with time_it("jinja2"):
    result = t.render(**params)

settings.configure(TEMPLATES=[
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': ['.'],  # if you want the templates from a file
        'APP_DIRS': False,  # we have no apps
    },
])
d_template = """\
<h3> This is the start of my child template</h3>
<br>
<p>My string: {{my_string}}</p>
<p>Value from the list: {{my_list.3}}</p>
<p>Loop through the list:</p>
<ul>
  {% for n in my_list %}<li>{{n}}</li>
  {% endfor %}
</ul>
<h3> This is the end of my child template</h3>
"""
django.setup()

t = template.Template(d_template)
with time_it("django"):
    c = template.Context(params)
    t.render(c)


def plant_a_html(x):
    with x.h3():
        x.text("This is the start of my child template")
    x.br()
    with x.p():
        x.text("My string: {}".format(my_string))
    with x.p():
        x.text("Value from the list: {}".format(my_list[3]))
    with x.p():
        x.text("Loop through the list:")
    with x.ul():
        for entry in my_list:
            with x.li():
                x.text(entry)
    with x.h3():
        x.text("This is the end of my child template")


with time_it("xplant", 8500):
    x = html5_plant()
    plant_a_html(x)

    result = str(x)

assert result == """\
<!DOCTYPE html>
<h3>This is the start of my child template</h3>
<br />
<p>My string: the numbers are: 0x1000, 0x1003, 0x1006, 0x1009, 0x100c, 0x100f, 0x1012, 0x1015, 0x1018, \
0x101b, 0x101e, 0x1021, 0x1024, 0x1027, 0x102a, 0x102d, 0x1030, 0x1033, 0x1036, 0x1039, 0x103c, 0x103f, \
0x1042, 0x1045, 0x1048, 0x104b, 0x104e, 0x1051, 0x1054, 0x1057, 0x105a, 0x105d, 0x1060, 0x1063, 0x1066, \
0x1069, 0x106c, 0x106f, 0x1072, 0x1075, 0x1078, 0x107b, 0x107e, 0x1081, 0x1084, 0x1087, 0x108a, 0x108d, \
0x1090, 0x1093, 0x1096, 0x1099, 0x109c, 0x109f, 0x10a2, 0x10a5, 0x10a8, 0x10ab, 0x10ae, 0x10b1, 0x10b4, \
0x10b7, 0x10ba, 0x10bd, 0x10c0, 0x10c3, 0x10c6, 0x10c9, 0x10cc, 0x10cf, 0x10d2, 0x10d5, 0x10d8, 0x10db, \
0x10de, 0x10e1, 0x10e4, 0x10e7, 0x10ea, 0x10ed</p>
<p>Value from the list: item #03: 4105 is 0x1009 in hex</p>
<p>Loop through the list:</p>
<ul>
  <li>item #00: 4096 is 0x1000 in hex</li>
  <li>item #01: 4099 is 0x1003 in hex</li>
  <li>item #02: 4102 is 0x1006 in hex</li>
  <li>item #03: 4105 is 0x1009 in hex</li>
  <li>item #04: 4108 is 0x100c in hex</li>
  <li>item #05: 4111 is 0x100f in hex</li>
  <li>item #06: 4114 is 0x1012 in hex</li>
  <li>item #07: 4117 is 0x1015 in hex</li>
  <li>item #08: 4120 is 0x1018 in hex</li>
  <li>item #09: 4123 is 0x101b in hex</li>
  <li>item #10: 4126 is 0x101e in hex</li>
  <li>item #11: 4129 is 0x1021 in hex</li>
  <li>item #12: 4132 is 0x1024 in hex</li>
  <li>item #13: 4135 is 0x1027 in hex</li>
  <li>item #14: 4138 is 0x102a in hex</li>
  <li>item #15: 4141 is 0x102d in hex</li>
  <li>item #16: 4144 is 0x1030 in hex</li>
  <li>item #17: 4147 is 0x1033 in hex</li>
  <li>item #18: 4150 is 0x1036 in hex</li>
  <li>item #19: 4153 is 0x1039 in hex</li>
  <li>item #20: 4156 is 0x103c in hex</li>
  <li>item #21: 4159 is 0x103f in hex</li>
  <li>item #22: 4162 is 0x1042 in hex</li>
  <li>item #23: 4165 is 0x1045 in hex</li>
  <li>item #24: 4168 is 0x1048 in hex</li>
  <li>item #25: 4171 is 0x104b in hex</li>
  <li>item #26: 4174 is 0x104e in hex</li>
  <li>item #27: 4177 is 0x1051 in hex</li>
  <li>item #28: 4180 is 0x1054 in hex</li>
  <li>item #29: 4183 is 0x1057 in hex</li>
  <li>item #30: 4186 is 0x105a in hex</li>
  <li>item #31: 4189 is 0x105d in hex</li>
  <li>item #32: 4192 is 0x1060 in hex</li>
  <li>item #33: 4195 is 0x1063 in hex</li>
  <li>item #34: 4198 is 0x1066 in hex</li>
  <li>item #35: 4201 is 0x1069 in hex</li>
  <li>item #36: 4204 is 0x106c in hex</li>
  <li>item #37: 4207 is 0x106f in hex</li>
  <li>item #38: 4210 is 0x1072 in hex</li>
  <li>item #39: 4213 is 0x1075 in hex</li>
  <li>item #40: 4216 is 0x1078 in hex</li>
  <li>item #41: 4219 is 0x107b in hex</li>
  <li>item #42: 4222 is 0x107e in hex</li>
  <li>item #43: 4225 is 0x1081 in hex</li>
  <li>item #44: 4228 is 0x1084 in hex</li>
  <li>item #45: 4231 is 0x1087 in hex</li>
  <li>item #46: 4234 is 0x108a in hex</li>
  <li>item #47: 4237 is 0x108d in hex</li>
  <li>item #48: 4240 is 0x1090 in hex</li>
  <li>item #49: 4243 is 0x1093 in hex</li>
  <li>item #50: 4246 is 0x1096 in hex</li>
  <li>item #51: 4249 is 0x1099 in hex</li>
  <li>item #52: 4252 is 0x109c in hex</li>
  <li>item #53: 4255 is 0x109f in hex</li>
  <li>item #54: 4258 is 0x10a2 in hex</li>
  <li>item #55: 4261 is 0x10a5 in hex</li>
  <li>item #56: 4264 is 0x10a8 in hex</li>
  <li>item #57: 4267 is 0x10ab in hex</li>
  <li>item #58: 4270 is 0x10ae in hex</li>
  <li>item #59: 4273 is 0x10b1 in hex</li>
  <li>item #60: 4276 is 0x10b4 in hex</li>
  <li>item #61: 4279 is 0x10b7 in hex</li>
  <li>item #62: 4282 is 0x10ba in hex</li>
  <li>item #63: 4285 is 0x10bd in hex</li>
  <li>item #64: 4288 is 0x10c0 in hex</li>
  <li>item #65: 4291 is 0x10c3 in hex</li>
  <li>item #66: 4294 is 0x10c6 in hex</li>
  <li>item #67: 4297 is 0x10c9 in hex</li>
  <li>item #68: 4300 is 0x10cc in hex</li>
  <li>item #69: 4303 is 0x10cf in hex</li>
  <li>item #70: 4306 is 0x10d2 in hex</li>
  <li>item #71: 4309 is 0x10d5 in hex</li>
  <li>item #72: 4312 is 0x10d8 in hex</li>
  <li>item #73: 4315 is 0x10db in hex</li>
  <li>item #74: 4318 is 0x10de in hex</li>
  <li>item #75: 4321 is 0x10e1 in hex</li>
  <li>item #76: 4324 is 0x10e4 in hex</li>
  <li>item #77: 4327 is 0x10e7 in hex</li>
  <li>item #78: 4330 is 0x10ea in hex</li>
  <li>item #79: 4333 is 0x10ed in hex</li>
</ul>
<h3>This is the end of my child template</h3>
""", result

"""
Results at version 0.3 are:

jinja2 took 43 microseconds
django took 605 microseconds
xplant took 3060 microseconds
yattag took 8078 microseconds

"""

with time_it("yattag"):
    y = yattag.SimpleDoc()
    with y.tag("h3"):
        y.text("This is the start of my child template")
    y.tag("br")
    with y.tag("p"):
        y.text("My string: {}".format(my_string))
    with y.tag("p"):
        y.text("Value from the list: {}".format(my_list[3]))
    with y.tag("p"):
        y.text("Loop through the list:")
    with y.tag("ul"):
        for entry in my_list:
            with y.tag("li"):
                y.text(entry)
    with y.tag("h3"):
        y.text("This is the end of my child template")

    yattag.indent(y.getvalue())

pr = cProfile.Profile()
pr.enable()

x = html5_plant()

plant_a_html(x)
plant_a_html(x)
plant_a_html(x)

pr.disable()
s = StringIO.StringIO()
ps = pstats.Stats(pr, stream=s).sort_stats('cumulative')
ps.print_stats()
